# MSc_Project

Software implementation for the MSc Project "Explainable Machine Learning for Outcome Prediction of Intracranial Aneurysm Treatment by Flow Diverters".

This repository contains four notebooks each with a Machine Learning model and the dataset which they used. The dataset exists in its original (before data preparation) and modified version that is used with the models.

To use the notebooks, download the notebook and upload it on Google Colab. The notebooks may run on other Jupyter notebook environments but Google Colab is recommended since the notebooks have been implemented and tested in that environment.

To use the dataset provided, please upload the "primary_features.csv" file in Colab's runtime environment to use with the desired model.